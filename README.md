# Calculator

An over-engineered react calculator for fun and practice.

## Features:

- [x] Keyboard input

## Upcoming features:

- [ ] Dark mode
- [ ] History
- [ ] Multiple Types
- [ ] Reducer based inner state
- [ ] Block invalid input
- [ ] Error Messages
- [ ] PWA
  - [ ] Responsive on small screens
  - [ ] Offline mode
